'use strict';

/**
 * @ngdoc service
 * @name 02ScaffoldingApp.powerCalc
 * @description
 * # powerCalc
 * Service in the 02ScaffoldingApp.
 */
angular.module('02ScaffoldingApp')
  .service('powerCalc', function () {
    // AngularJS will instantiate a singleton by calling "new" on this function

    function add(a, b) {
      return a + b;
    }
    
    var api  = {
      add: add
    };

    return api;


  });
