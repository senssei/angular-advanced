'use strict';

angular.module('02ScaffoldingApp')
  .controller('MainCtrl', function (MAGIC_STRING, powerCalc, $filter, $http ) {
    var vm = this;
    
    vm.letters = ['a', 'b', 'c'];
    vm.magic = MAGIC_STRING;
    
    vm.func = powerCalc.add;
    
    vm.numbers = [1,2,3,4,5];
    
    vm.val = $filter('plCurrency')(this.numbers[4]);
    
    vm.names = [];
      
    $http.get('api/data.json').success(function(response) {
      vm.names = response.records;
    });
    
    
  });
