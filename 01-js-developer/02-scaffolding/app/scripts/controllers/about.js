'use strict';

/**
 * @ngdoc function
 * @name 02ScaffoldingApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the 02ScaffoldingApp
 */
angular.module('02ScaffoldingApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
